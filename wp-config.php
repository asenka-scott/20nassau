<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', '20nassau');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'D*6Aj_|T72q9*xL*tTT$*j#ftYoFJPqTS~]/~7v[,YmdI];X7vEL)A%enc84hd?G');
define('SECURE_AUTH_KEY',  '|!zNkQD$8~V%[rCi=mS,7jqlh1I3Ov.ZGN}yd(z6-#_*JfQh=1>wkb``@18$#QiB');
define('LOGGED_IN_KEY',    '@LmLP(y#Mx=#vBtI$9?C/Obrnrl,m%sB=9&TX pIxJPl`2yN^C(GeLR3M+`Oc*Y1');
define('NONCE_KEY',        '%-P!M5%^*yOn[#R@%]Lo%sS3M}L^^Fsrskyqd*j fB9.7$K.-PE>uE,.lNc]W;,#');
define('AUTH_SALT',        'NcP&WE{BhT@uN,ZtBv@}R9*%M;|eb+cjoZ@Edur?$E!QJGgFG&H#9[m]H2Q]V7XH');
define('SECURE_AUTH_SALT', 'J&=Y*$-of N[50)XU5jDZ*ib;Ney3:i f$jH?VGl$mwawO)+bEW9{xo|QnxdYIJ@');
define('LOGGED_IN_SALT',   'MV@A8nf(#y_5Nwyv39,bvQ5 I#2Q_=w(&@uTa%ek:h||~ZE{Oks]l0]]-*<1PrM]');
define('NONCE_SALT',       'w[H-H]|)hioX}X-7HJs#+b1n9,NL/saYRT.7$zmRcrAQc7xIPAnxYRC{$g<6}u+A');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
